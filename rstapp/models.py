from django.db import models

# Create your models here.


class Presentation(models.Model):
    name = models.CharField(max_length=200)
    content = models.CharField(max_length=99999)
    style = models.CharField(max_length=100)
    pub_date = models.DateTimeField('date published')

    def __str__(self):
        return self.name
