from django.conf.urls import url

from . import views

app_name = 'rstapp'

urlpatterns = [
    url(r'^$', views.HomeView.as_view(), name='home'),
    url(r'create_rst$', views.create_rst, name='create_rst'),
    url(r'presentation_list$', views.present_all_presentation, name='presentation_list'),
    url(r'upload_pic', views.upload_pic, name='upload_pic'),
]