rst2html5
=========

* what is it?
* who created it?
* what does it do?
* status
* future
* contribute

what is it?
===========

* a program that transforms restructured text
* into html5 markup with css3 styles
* that allows to post process it

who created it?
===============

* Mariano Guerra

hi!
---

.. image:: /static/rstapp/images/url.jpg