from django.shortcuts import render
from django.views import generic
from django.http import HttpRequest , HttpResponse , HttpResponseRedirect
from django.template import loader
from django.core.urlresolvers import reverse
from django.core.files.storage import default_storage
from .models import Presentation
from django.utils import timezone
from django.http import JsonResponse
from django.core import serializers
from django.forms.models import model_to_dict
import json

import os


class HomeView(generic.ListView):
    template_name = 'rstapp/home.html'

    def get_queryset(self):

        return None


def create_rst(request):

    # Get file form HTML
    file_name = request.POST["presentation_name"]

    if not Presentation.objects.filter(name=file_name):
        new_file = Presentation(name=file_name, pub_date=timezone.now())
        new_file.save()

    rst_dir_path = 'rstapp/static/rstapp/rst_files'
    presentation_html_dir_path = "rstapp/static/rstapp/rst_to_html_files"
    presentation_style = request.POST["style_presentation"]

    # Write file
    content = request.POST["content"]
    rst_file = open('{}/{}.rst'.format(rst_dir_path, file_name), 'w')
    rst_file.write(content)
    rst_file.close()

    # Create presentation
    os.system('rst2html5 {} {}/{}.rst > {}/{}.html'.format(presentation_style, rst_dir_path, file_name,
                                                           presentation_html_dir_path, file_name))
    # Open presentation
    html_file = open("{}/{}.html".format(presentation_html_dir_path, file_name), "r")
    html_text = html_file.read()

    # Picture Upload

    if request.FILES:
        save_pic = request.FILES['upload']
        save_pic_dir = 'rstapp/static/rstapp/images/'
        save_pic_path = default_storage.save(save_pic_dir, save_pic)
        os.rename(save_pic_path, "{}{}".format(save_pic_dir, request.FILES['upload'].name))

    return HttpResponse(html_text)


def present_all_presentation(request):
    latest_presentation_list = Presentation.objects.order_by('pub_date')

    list_name = []
    for name in latest_presentation_list:
        list_name.append(name.name)

    list_name_json = json.dumps(list_name)

    # dict_obj = model_to_dict(latest_presentation_list)
    # latest_presentation_list_serialized = serializers.serialize('json', latest_presentation_list)
    # serialized = json.dumps(dict_obj)
    # presentation_list = JsonResponse(latest_presentation_list_serialized, safe=False)

    # return render(request, 'rstapp/presentation_list.html', presentation_list)
    return render(request, 'rstapp/presentation_list.html', {'latest_presentation_list': list_name_json})
    # return HttpResponse(json.dumps(latest_presentation_list_serialized))
    # return JsonResponse(latest_presentation_list_serialized, safe=False)
    # return HttpResponse(serialized)


def upload_pic(request):
    data = {}
    return JsonResponse(data)


def upload_file(request):
    if request.FILES:
        rst_dir_path = 'rstapp/static/rstapp/rst_files'
        presentation_html_dir_path = "rstapp/static/rstapp/rst_to_html_files"

        '''csv_file = request.FILES['csvfile']
        file_path = default_storage.save(dir_path, csv_file)
        os.rename(file_path, "{}word_bank.csv".format(dir_path))'''

